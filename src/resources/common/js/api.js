import axios from 'axios';
import humps from 'humps';
import qs from 'qs'

export const fileUploadLink = window.location.origin + '/api/admin/files';
export const http = axios.create({
    baseURL: window.location.origin + '/api/admin/',
});
window.fileUploadLink = fileUploadLink;
window.http = http;

http.interceptors.request.use(function (config) {
    if (config.humps === false) return config;

    if (config.params) {
        config.params = humps.decamelizeKeys(config.params);

        config.paramsSerializer = function (params) {
            return qs.stringify(config.params, {arrayFormat: 'index'});
        }
    }

    if (config.data)
        config.data = humps.decamelizeKeys(config.data);

    return config;
}, function (error) {
    return Promise.reject(error);
});

http.interceptors.response.use(function (response) {
    if (response.config.humps === false) return response;

    return humps.camelizeKeys(response);
}, function (error) {
    // if (error.response.status === 401) store.dispatch('auth/logout');
    return Promise.reject(error.response);
});

export const resources = {
    route: 'resources',
    async fetch(params) {
        return (await http.get(`${this.route}`, {params: params})).data;
    },
    async fetchOne(id, params) {
        return (await http.get(`${this.route}/${id}`, {params: params})).data;
    },
    async store(params) {
        return (await http.post(`${this.route}`, params)).params;
    },
    async update(id, params) {
        return (await http.put(`${this.route}/${id}`, params)).data;
    },
    async delete(id, params) {
        return (await http.delete(`${this.route}/${id}`, params)).data;
    },
};
