<?php


namespace App\Services\EmailVerification\Controllers;


use App\Services\EmailVerification\Services\EmailVerificationService;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class EmailVerificationController extends Controller
{
    use ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function status(Request $request)
    {
        return Auth::user()->hasVerifiedEmail() ? 1 : 0;
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'email' => 'sometimes|email|unique:users,email,' . Auth::id(),
        ]);

        if ($request->filled('email')) {
            Auth::user()->email = $request->input('email');
            Auth::user()->email_verified_at = null;
            Auth::user()->save();
        }

        if (Auth::user()->hasVerifiedEmail()) return 0;

        switch ($request->input('type', 'code')) {
            case 'code':
                app(EmailVerificationService::class)->sendVerificationCodeNotification();
                break;
            case 'redirect':
                app(EmailVerificationService::class)->sendVerificationRedirectNotification();
                break;
        }
        return 1;
    }

    public function verifyCode(Request $request)
    {
        app(EmailVerificationService::class)->verifyCode($request->input('code'));
        Auth::user()->markEmailAsVerified();
        return Auth::user()->email;
    }

    public function verifyRedirect(Request $request)
    {
        try {
            app(EmailVerificationService::class)->verifyRedirect($request);
        } catch (ValidationException $e) {
            return view('mail-verification-result', ['result' => 'fail']);
        }

        Auth::user()->markEmailAsVerified();
        return view('mail-verification-result', ['result' => 'success']);
    }
}
