<?php /** @noinspection PhpMissingFieldTypeInspection */

namespace App\Services\Stats\Models;

use Illuminate\Database\Eloquent\Model;

class Stats extends Model
{
    protected $table = 'statistics';
    public $timestamps = null;
    protected $fillable = ['date', 'object_type', 'object_id', 'count'];
    protected $hidden = ['object_id', 'object_type', 'id'];
//    protected $casts = ['date' => 'date'];
}
