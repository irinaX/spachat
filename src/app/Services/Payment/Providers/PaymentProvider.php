<?php

namespace App\Services\Payment\Providers;

use App\Services\Payment\Controllers\PaymentController;
use App\Services\Payment\Interfaces\PaymentInterface;
use App\Services\Payment\Services\TransactionService;
use App\Services\Payment\Services\YandexKassaService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class PaymentProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(TransactionService::class)
            ->needs('$payer')
            ->give(function () {
                return Auth::user();
            });

        $this->app->when(YandexKassaService::class)
            ->needs('$payer')
            ->give(function () {
                return Auth::user();
            });

        $this->app->bind(PaymentInterface::class, YandexKassaService::class);

        $this->app->bind(TransactionService::class, TransactionService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Migrations');
        Route::post('payment-hook', PaymentController::class . '@resolveWebHook');
    }
}
