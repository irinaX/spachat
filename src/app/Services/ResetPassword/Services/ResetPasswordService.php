<?php


namespace App\Services\ResetPassword\Services;


use App\Services\ResetPassword\Interfaces\ResetPasswordInterface;
use App\Services\ResetPassword\Notifications\ResetPasswordNotification;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;

class ResetPasswordService
{
    const RESET_WITH_EMAIL = 'email';
    const RESET_WITH_PHONE = 'phone';

    public function send(ResetPasswordInterface $user, $type)
    {
        $code = rand(100000, 999999);
        $cacheKey = $this->getCacheKey($user);
        Cache::put($cacheKey, [
            $code, get_class($user), $user->getKey()
        ], 300);

        Notification::send($user, new ResetPasswordNotification($type, $code));
        return $cacheKey;
    }

    public function verify($token, $code): ResetPasswordInterface
    {
        $value = Cache::get($token);
        if ($value && $value[0] == $code)
            return (new $value[1])->findOrFail($value[2]);
        else
            throw ValidationException::withMessages(['code' => 'Неправильный код подтверждения']);
    }

    private function getCacheKey(ResetPasswordInterface $user): string
    {
        return md5('reset_password' . get_class($user) . $user->getPasswordField());
    }
}
