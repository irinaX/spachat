import router from './router';
import App from './components/App';
import Vue from 'vue';
//import moment from "moment";// глобальное подключение библиотек
//window.moment = moment;

// window.moment = require('moment'); устаревший синтаксис
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

new Vue({
    el: '#app',
    router,
    render: h => h(App),
});



