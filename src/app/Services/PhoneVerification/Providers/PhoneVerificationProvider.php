<?php

namespace App\Services\PhoneVerification\Providers;

use App\Services\PhoneVerification\Interfaces\PhoneVerificationInterface;
use App\Services\PhoneVerification\Services\PhoneVerificationService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class PhoneVerificationProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PhoneVerificationService::class);
        $this->app->when(PhoneVerificationService::class)
            ->needs(PhoneVerificationInterface::class)
            ->give(function () {
                return Auth::user();
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::group([
            'prefix' => 'api/user/verification',
            'middleware' => 'api',
            'namespace' => 'App\Services\PhoneVerification\Controllers'
        ], function () {
            Route::get('phone/status', 'PhoneVerificationController@status');
            Route::get('phone', 'PhoneVerificationController@send');
            Route::post('phone', 'PhoneVerificationController@verify');
        });
    }
}
