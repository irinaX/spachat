<?php


namespace App\Services\Stats\Services;


use App\Services\Stats\Interfaces\StatsInterface;
use App\Services\Stats\Models\Stats;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class StatsService implements StatsInterface
{

    public function hit($model, string $type)
    {
        $id = is_numeric($model) ? $model : $model->id;
        $res = Stats::where('date', Carbon::now()->toDateString())
            ->where('object_type', $type)
            ->where('object_id', $id)
            ->increment('count');

        if ($res == 0) {
            Stats::create([
                'date' => Carbon::now(),
                'object_type' => $type,
                'object_id' => $id,
                'count' => 1,
            ]);
        }
    }

    private function makeGetQuery($id, $type, $from = null, $to = null): Builder
    {
        $query = Stats::orderBy('date', 'asc')
            ->where('object_type', $type)->where('object_id', $id);
        if ($from) $query->where('date', '>=', $from);
        if ($to) $query->where('date', '<=', $to);
        return $query;
    }

    public function getStats($model, string $type, $from = null, $to = null): Collection
    {
        $id = is_numeric($model) ? $model : $model->id;
        return $this->makeGetQuery($id, $type, $from, $to)->get();
    }

    public function getTopStats($model, string $type, string $field, $from = null, $to = null, int $count = 10): Collection
    {
        /** @var Builder $query */
        $query = call_user_func([$model, 'query']);
        $tableName = (new $model)->getTable();
        $query->join('statistics', function ($join) use ($to, $from, $type, $tableName) {
            $join->on($tableName . '.id', '=', 'statistics.object_id')
                ->where('object_type', $type);
            if ($from) $join->where('date', '>=', $from);
            if ($to) $join->where('date', '<', $to);
        })
            ->select(['activities.*', DB::raw('CAST(SUM(statistics.count) AS SIGNED) as ' . $field)])
            ->orderBy($field, 'desc')
            ->groupBy('activities.id')
            ->take($count);

        return $query->get();
    }

    public
    function total($model, string $type, $from = null, $to = null): int
    {
        $id = is_numeric($model) ? $model : $model->id;
        return $this->makeGetQuery($id, $type, $from, $to)->sum('count');
    }
}
