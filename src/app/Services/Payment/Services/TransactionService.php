<?php


namespace App\Services\Payment\Services;


use App\Services\Payment\Interfaces\InteractsWithPaymentsInterface;
use App\Services\Payment\Models\Transaction;
use InvalidArgumentException;

class TransactionService
{
    protected ?InteractsWithPaymentsInterface $payer;

    public function __construct(InteractsWithPaymentsInterface $payer = null)
    {
        $this->payer = $payer;
    }

    public function push(float $amount, string $description = null): Transaction
    {
        if ($amount == 0) throw new InvalidArgumentException('Field amount in transaction can not be null.');

        $lastTransaction = $this->payer->transactions()->latest()->first();
        $lastCredit = $lastTransaction ? $lastTransaction->credit : 0;
        $newCredit = $lastCredit + $amount;

        return Transaction::create([
            'owner_type' => get_class($this->payer),
            'owner_id' => $this->payer->getPayerId(),
            'type' => $amount > 0 ? 'income' : 'debit',
            'amount' => abs($amount),
            'credit' => $newCredit,
            'description' => $description,
        ]);
    }

    public function canPay(float $amount): bool
    {
        $lastTransaction = $this->payer->transactions()->latest()->first();
        $lastCredit = $lastTransaction ? $lastTransaction->credit : 0;

        return $lastCredit >= $amount;
    }
}
