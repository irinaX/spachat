<?php


namespace App\Utils;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Trait BuildQuery
 * @package App\Http\Controllers\Utils
 * @method static Builder|self buildQuery(Request $request)
 */
trait BuildQuery
{
    public function scopeBuildQuery(Builder $query, $r)
    {
        foreach ($r->with ?? [] as $field) {
            $methodName = 'with' . Str::studly($field);

            if (method_exists($this, $methodName)) {
                $this->{$methodName}($query);
            }
        }

        if ($this->{$this->getKeyName()}) {
            $query->eagerLoadRelations([$this]);
            return $this;
        } else {
            if ($r->offset) {
                $query->offset($r->offset);
            }

            if ($r->limit) {
                $query->limit($r->limit);
            }

            if ($r->order_by) {
                foreach ($r->order_by as $rule) {
                    $rule = explode(':', $rule);

                    $method = 'orderBy' . Str::studly($rule[0]);

                    if (method_exists($this, $method)) {
                        $this->{$method}($query);
                    } else {
                        $query->orderBy($rule[0], $rule[1] ?? 'asc');
                    }
                }
            }

            if ($r->filters) {
                foreach ($r->filters as $params) {
                    if (!is_array($params)) {
                        $params = json_decode($params);
                    }

                    $method = 'filter' . Str::studly($params[0]);

                    if (method_exists($this, $method)) {
                        $this->{$method}($query, ...array_slice($params, 1));
                    } else {
                        $query->where(...$params);
                    }
                }
            }
        }

        return $query;
    }
}
