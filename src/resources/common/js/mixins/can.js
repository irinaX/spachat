export default {
    methods: {
        can(...abilities) {
            return checkAbilities(abilities, this.$store.state.auth.permissions)
        }
    }
}

export function checkAbilities(abilities, inArray) {
    const abilitiesArray = abilities.reduce((acc, item) => {
        if (Array.isArray(item)) acc.push(...item);
        else acc.push(item);
        return acc;
    }, []);

    try {
        return abilitiesArray.every(item => inArray.includes(item));
    } catch (e) {
        console.error(e);
        return false;
    }
}

