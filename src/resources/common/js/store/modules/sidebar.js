const state = {
    expanded: localStorage.getItem('sidebar_expand') === 'true',
};

const mutations = {
    toggle(state, val) {
        val = val === undefined ? !state.expanded : val;
        state.expanded = val;
        localStorage.setItem('sidebar_expand', val)
    },
};

export const sidebar = {
    namespaced: true,
    state,
    mutations,
};
