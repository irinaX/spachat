const mix = require('laravel-mix');
const config = require('./webpack.config');
require('laravel-mix-polyfill');

mix.webpackConfig(config);
mix.disableSuccessNotifications();
mix.options({processCssUrls: false});

mix.polyfill({
    enabled: true,
    useBuiltIns: "usage",
    targets: {"firefox": "50", "ie": 11}
})

mix.js('resources/common/js/app.js', 'public/static/common/js')
mix.sass('resources/common/sass/app.scss', 'public/static/common/css');
