<?php

namespace App\Services\Attachment\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;

class AttachmentResource extends JsonResource
{

    public function toArray($request)
    {
        $url = $this->type != 'embed' ?
            Storage::disk($this->disk)->url($this->original) : $this->original;
        $thumbnail = $this->thumbnail ?
            Storage::disk($this->disk)->url($this->thumbnail) : null;
        return [
            'id' => $this->id,
            'name' => $this->name,
            'alt' => $this->alt,
            'type' => $this->type,
            'original' => $url,
            'thumbnail' => $this->when($thumbnail, $thumbnail),
            'size' => $this->size,
            'order_column' => $this->order_column,
            'created_at' => $this->created_at,
        ];
    }
}
