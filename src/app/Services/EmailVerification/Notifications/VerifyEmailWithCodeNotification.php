<?php

namespace App\Services\EmailVerification\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VerifyEmailWithCodeNotification extends Notification
{

    private int $code;

    public function __construct(int $code)
    {
        $this->code = $code;
    }

    /**
     * Get the notification's channels.
     *
     * @param mixed $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Подтвердите регистрацию')
            ->line('Ваш код подтверждения email на сайте razvivaites.ru: ' . $this->code)
            ->line('Просто проигнорируйте это письмо если вы не регистрировались на сайте.');
    }
}
