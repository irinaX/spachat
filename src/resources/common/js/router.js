import Vue from 'vue';
import VueRouter from 'vue-router';
import Page1 from "./components/Page1";
import Page2 from "./components/Page2";

Vue.use(VueRouter);

export const routes = [
    {
        path: '/page-1',
        component: Page1,
        name: 'page-1'
    },
    {
        path: '/page-2',
        component: Page2,
        name: 'page-2'
    },
];

const router = new VueRouter({
    mode: 'history',
    routes: routes
});


export default router;
