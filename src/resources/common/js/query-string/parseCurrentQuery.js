import qs from "qs";

export default function () {
    try {
        const splitHref = window.location.href.split('?', 2);
        return qs.parse(splitHref[1]);
    } catch (e) {
        return {};
    }
}
