<?php


namespace App\Services\EmailVerification\Services;

use App\Services\EmailVerification\Interfaces\EmailVerificationInterface;
use App\Services\EmailVerification\Notifications\VerifyEmailRedirectNotification;
use App\Services\EmailVerification\Notifications\VerifyEmailWithCodeNotification;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\ValidationException;

class EmailVerificationService
{
    /**
     * @var EmailVerificationInterface
     */
    private EmailVerificationInterface $notifiable;
    private string $cacheKey;

    public function __construct(EmailVerificationInterface $notifiable)
    {
        $this->notifiable = $notifiable;
        $this->cacheKey = md5('email_verification' . get_class($this->notifiable) . $this->notifiable->getKey());
    }

    public function sendVerificationCodeNotification()
    {
        $code = rand(100000, 999999);
        Cache::put($this->cacheKey, $code, 300);
        Notification::send($this->notifiable, new VerifyEmailWithCodeNotification($code));
    }

    public function sendVerificationRedirectNotification()
    {
        $hours = 3;
        Cache::put($this->cacheKey, $this->notifiable, $hours * 60 * 60);
        $signedRoute = URL::temporarySignedRoute(
            'email-verification.verify-redirect', now()->addHours($hours), ['hash' => $this->cacheKey]
        );
        Notification::send($this->notifiable, new VerifyEmailRedirectNotification($signedRoute));
    }

    public function verifyCode($code)
    {
        if ($this->notifiable->hasVerifiedEmail())
            throw ValidationException::withMessages(['code' => 'Email уже подтверждён.']);
        else if (Cache::get($this->cacheKey) != $code)
            throw ValidationException::withMessages(['code' => 'Неверный код подтверждения.']);
        return true;
    }

    public function verifyRedirect($request)
    {
        $notifiable = Cache::get($request->input('hash'));
        if (!$request->hasValidSignature() || !$notifiable) {
            throw ValidationException::withMessages(['code' => 'Ссылка устарела или неправильная.']);
        }
        if ($notifiable->hasVerifiedEmail())
            throw ValidationException::withMessages(['code' => 'Email уже подтверждён.']);
        return true;
    }
}
