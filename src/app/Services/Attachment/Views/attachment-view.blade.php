@if($attachment->type == 'embed')
    {!! $attachment->url !!}
@else
    <img src="{{ $attachment->url }}" alt="{{ $attachment->alt }}" {{ $attributes }}/>
@endif
