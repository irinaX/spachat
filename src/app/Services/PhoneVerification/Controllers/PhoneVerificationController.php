<?php


namespace App\Services\PhoneVerification\Controllers;


use App\Services\PhoneVerification\Services\PhoneVerificationService;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class PhoneVerificationController extends Controller
{
    use ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function status(Request $request)
    {
        return Auth::user()->hasVerifiedPhone() ? 1 : 0;
    }

    public function send(Request $request)
    {
        if ($request->has('phone')) {
            $phone = $request->input('phone');
            $request->merge([
                'phone' => preg_replace('/[^0-9]/', '', $phone)
            ]);
        }
        $this->validate($request, [
            'phone' => 'sometimes|regex:/^((7)+([0-9]){10})$/|unique:users,phone,' . Auth::id(),
        ]);

        if ($request->filled('phone')) {
            Auth::user()->phone = $request->input('phone');
            Auth::user()->phone_verified_at = null;
            Auth::user()->save();
        }

        if (Auth::user()->hasVerifiedPhone()) return 0;

        app(PhoneVerificationService::class)->sendVerificationNotification();
        return 1;
    }

    public function verify(Request $request)
    {
        app(PhoneVerificationService::class)->verify($request->input('code'));
        Auth::user()->markPhoneAsVerified();
        return Auth::user()->phone;
    }
}
