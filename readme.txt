echo "NGINX_PORT=8080:" > .env
cp ./src/.env.example ./src/.env
chmod 777 -R ./src/storage
chmod 777 -R ./src/bootstrap
composer install
php artisan key:generate